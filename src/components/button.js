import styled from 'styled-components'
import theme from "./theme";

const Button = styled.button`
    transition: all .3s ease;
    background: ${props => theme.colors[Object.keys(props).find(p => theme.colors[p])] || theme.colors.primary};
    text-transform: ${props => props.upper ? 'uppercase' : 'none'};
    font-weight: 600;
    color: ${theme.colors.grayLight};
    padding: 1rem 1.5rem;
    border: none;
    border-radius: 100rem;
    cursor: pointer;
    margin: 1rem;
    width: ${props => props.large ? '100%' : 'auto'};
    &:hover {
        opacity: .7;
    }
`;

export default Button