import axios from 'axios'
import api from '../../utils/services'

export const loadCompanies = () => {
  return axios.get(api.url + '/companies')
    .then(res => res.data)
    .catch(err => err.response.data)
};
