import React from 'react';
import styled from 'styled-components'
import theme from "./theme";

const Select = styled.select`
    transition: all .3s ease;
    font-weight: 300;
    color: ${theme.colors.default};
    padding: 9px 13px;
    margin: 1px;
    border-radius: 100rem;
    cursor: pointer;
    width: 100%;
    max-width: 400px;
    &:focus {
        outline: none;
    }
`;

const Option = styled.option`
    transition: all .3s ease;
    font-weight: 300;
    color: ${theme.colors.default};
    padding: 9px 13px;
    margin: 1px;
    cursor: pointer;
    width: 100%;
    &:focus {
        outline: none;
    }
`;

const Search = (props) => {
    return (
        <Select onChange={(e) => props.onChange(e.target.value)}>
            <Option defaultValue value={0}>{props.default}</Option>
            {
                props.options &&
                props.options.map((option, index) =>
                    <Option
                        value={option}
                        key={index}>
                        {option}
                    </Option>
                )
            }
        </Select>
    )
}

export default Search