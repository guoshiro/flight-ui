export const LIST_COMPANIES = 'companies/LIST_COMPANIES';

export const listCompanies = payload => ({
  type: LIST_COMPANIES,
  payload: payload
});
