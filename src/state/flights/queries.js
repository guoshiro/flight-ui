import axios from 'axios'
import api from '../../utils/services'
import { orderByList } from '../../constants';

export const loadFlights = (data) => {
  return axios.post(api.url, { ...data })
    .then(res => res.data)
    .catch(err => err.response.data)
};


export const orderFlight = (objectArray, prop) => {
  objectArray.map(object => {
    object.valorTotal = 0;
    object.tempoTotal = 0;
    object.voos.map(fly => {
      object.valorTotal += fly.valor;
      object.tempoTotal += setHours(fly.data_saida, fly.chegada) - setHours(fly.data_saida, fly.saida);
    })
  })
  switch (prop) {
    case orderByList[0]:
      return objectArray.sort((a, b) => (a.valorTotal > b.valorTotal) ? 1 : ((b.valorTotal > a.valorTotal) ? -1 : 0));;
      break;
    case orderByList[1]:
      return objectArray.sort((a, b) => (a.tempoTotal > b.tempoTotal) ? 1 : ((b.tempoTotal > a.tempoTotal) ? -1 : 0));;
      break;
    default:
      break;
  }
}

const setHours = (date, hour) => {
  return new Date(date, hour).getHours()
}