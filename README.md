# Flight-ui

Esse projeto possui os seguintes stacks:
- React
- Context
- Hooks (useReducer, useContext)
- State Global com Context + Hooks
- Custom Hooks (Como redux Thunks)
- Axios
- Styled-component com GlobalStyle

# Design inspirado em

https://dribbble.com/shots/4318286-Roundlaw-website/attachments/984362

# Build

Para executar, primeiro execute:

```npm install```

Depois:

```npm start```

