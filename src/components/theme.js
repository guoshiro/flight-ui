const colors = {
  primary: '#dca266',
  grayDark: '#333',
  grayMedium: '#8d8d8d',
  grayMediumLight: '#dddddd',
  grayLight: '#fff',
  gradient: 'linear-gradient(112deg, rgba(113,89,193,1) 0%, rgba(155,73,193,1) 100%);',
  bg: 'linear-gradient(to top, #a18cd1 0%, #fbc2eb 100%);'
}

const boxShadow = {
  default: `3px 2px 5px ${colors.grayMediumLight}`,
  hover: `3px 2px 8px ${colors.grayMediumLight}`
}
const font = {
  ultraSmall: '0.8rem',
  small: '1rem',
  medium: '1.5rem',
  big: '2rem',
  title: '2.5rem',
}

const viewports = {
  smartphone: '360px',
  tablet: '720px',
  desktop: '1280px'
}

const theme = Object.freeze({
  colors,
  viewports,
  boxShadow,
  font
});

export default theme;