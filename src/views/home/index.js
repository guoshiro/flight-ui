import React, { useEffect } from 'react'
import useCompanies from '../../state/companies/hooks/useCompanies'
import useFlights from '../../state/flights/hooks/useFlights'
import useFilterFlights from '../../state/flights/hooks/useFilterFlights'

import tranformObjectListToList from '../../utils/Helpers/tranformObjectListToList'
import dateList from '../../utils/Helpers/dateList'

import Spinner from '../../components/spinner'
import Section from '../../components/section'
import Search from './containers/search'
import Airplanes from './containers/airplanes'

const Home = () => {
  const [companies, isLoading, setListCompanies] = useCompanies();
  const [flights, isLoad, setListFlights] = useFlights();
  const [setFilterListFlights] = useFilterFlights();
  let airportList;

  useEffect(() => {
    if (!companies.list) {
      setListCompanies();
    }
  }, []);

  if (companies.list) {
    airportList = tranformObjectListToList(companies.list, 'aeroporto')
  }

  const handlerItineraries = (e) => {
    setListFlights(e)
  }

  const handlerFilterFlights = (e) => {
    setFilterListFlights(flights.list, e)
  }

  return (
    <Section>
      <Spinner show={isLoading || isLoad} />
      <Search
        dateList={dateList}
        onSearchItineraries={e => handlerItineraries(e)}
        airportList={airportList}
      />
      {flights.list && <Airplanes flights={flights.list} handlerFilter={e => handlerFilterFlights(e)} />}
    </Section>
  )
};

export default Home
