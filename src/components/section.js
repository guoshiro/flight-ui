import styled from 'styled-components'
import theme from "./theme";

const Section = styled.section`
    transition: all .3s ease;
    padding: 0;
    display: flex; 
    overflow: hidden;
    max-height: 100vh;
`;

export default Section