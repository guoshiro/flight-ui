import companiesReducer from './companies/reducers';
import flightsReducer from './flights/reducers';

export default ({ companies, flights }, action) => ({
  companies: companiesReducer(companies, action),
  flights: flightsReducer(flights, action)
});