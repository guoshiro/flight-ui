import React, { useState } from 'react';
import styled from 'styled-components'
import theme from '../../../components/theme'
import Select from '../../../components/select'
import Button from '../../../components/button'

const Question = styled.h1`
    font-weight: 100;
    font-size: ${theme.font.medium};
    color: ${theme.colors.grayLight};
    margin: 2rem 0 1rem;
`

const Container = styled.div`
    background: ${theme.colors.grayDark};
    width: 100%;
    height: 100vh;
    display: flex;
    align-items: center;
    flex-direction: column;
    justify-content: center;
    padding: 5rem 2rem;
    transition: 0.2 all;
    overflow: hidden;
`;

const Search = props => {
    const [place] = useState({ from: 0, to: 0, date: 0 })

    const handlerObject = (value, key) => {
        place[key] = value;
    }

    const onSearchFly = () => {
        if (place['from'] == 0 || place['to'] == 0 || place['date'] == 0){
            alert('Favor preencher todos os campos')
        } else {
            props.onSearchItineraries(place)
        }
    }

    return (
        <Container>
            <Question>Qual é o aeroporto de origem?</Question>
            <Select
                onChange={e => handlerObject(e, 'from')}
                default={'Selecione o aeroporto de origem'}
                options={props.airportList} />
            <Question>E para onde você deseja ir?</Question>
            <Select
                onChange={e => handlerObject(e, 'to')}
                default={'Selecione o aeroporto de destino'}
                options={props.airportList} />
            <Question>Agora qual a data que planeja viajar?</Question>
            <Select
                onChange={e => handlerObject(e, 'date')}
                default={'Selecione uma data disponível'}
                options={props.dateList} />
            <Button upper onClick={() => onSearchFly()}>Pesquisar</Button>
        </Container>
    )

}

export default Search;