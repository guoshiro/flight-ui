import { useState } from 'react'
import { useStateValue } from '../../index'
import { loadFlights } from '../queries'
import { listFlights } from '../actions'

const useFlights = () => {
  const [{ flights }, dispatch] = useStateValue()
  const [isLoading, setIsLoading] = useState(false)

  const request = async (e) => {
    setIsLoading(true)

    const response = await loadFlights(e);

    if (response) {
      dispatch(listFlights(response))
    } else {
      const err = []
      dispatch(listFlights(err))
    }
    setIsLoading(false)
  }

  return [flights, isLoading, request]
}

export default useFlights
