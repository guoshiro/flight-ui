import React from 'react';
import styled from 'styled-components'
import theme from '../../../components/theme';

const Item = styled.div`
    box-shadow: ${theme.boxShadow.default};
    border-radius: 5px;
    padding: 1rem 1.5rem;
    margin: 1rem 0px;
    display: flex;
    transition: .2s all;
    >*{ 
        width: 33%
        margin: 0 10px;
    }
    :hover{
        box-shadow: ${theme.boxShadow.hover};
    }
`
const Top = styled.div``

const Bottom = styled.div``

const GroupInformation = styled.div`
    margin: 0 10px;
`

const Group = styled.div`
    display: flex;

`

const Label = styled.div`
    color: ${theme.colors.grayMedium};
    font-size: ${theme.font.ultraSmall};
    margin-top: 10px;
`

const Information = styled.div`
    color: ${theme.colors.grayDark};
    font-size: ${theme.font.small};
`

const Value = styled.div``

const Search = (props) => {
    return (
        <Item onChange={(e) => props.onChange(e.target.value)}>
            <div>
                <Top>
                    <Group>
                        <GroupInformation>
                            <Label>Origem</Label>
                            <Information>{props.fly.origem}</Information>
                        </GroupInformation>
                        <GroupInformation>
                            <Label>Destino</Label>
                            <Information>{props.fly.destino}</Information>
                        </GroupInformation>
                    </Group>
                </Top>
                <Bottom>
                    <GroupInformation>
                        <Label>Número do Vôo</Label>
                        <Information>{props.fly.voo}</Information>
                    </GroupInformation>
                </Bottom>
            </div>
            <div>
                <Top>
                    <GroupInformation>
                        <Label>Data de Saída</Label>
                        <Information>{props.fly.data_saida}</Information>
                    </GroupInformation>
                    <Group>
                        <GroupInformation>
                            <Label>Horário de Saída</Label>
                            <Information>{props.fly.saida}</Information>
                        </GroupInformation>
                        <GroupInformation>
                            <Label> Horário de Chegada</Label>
                            <Information>{props.fly.chegada}</Information>
                        </GroupInformation>
                    </Group>
                </Top>
            </div>
            <Value>R$ {props.fly.valor}</Value>
        </Item >
    )
}

export default Search