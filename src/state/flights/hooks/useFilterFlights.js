import { useState } from 'react'
import { useStateValue } from '../../index'
import { orderFlight } from '../queries'
import { listFlights } from '../actions'

const useFilterFlights = () => {
    const [{ flights }, dispatch] = useStateValue()

    const request = (list, e) => {
        const response = orderFlight(list, e);
        if (response) {
            dispatch(listFlights(response))
        } else {
            const err = []
            dispatch(listFlights(err))
        }
    }

    return [request]
}

export default useFilterFlights
