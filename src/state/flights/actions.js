export const LIST_FLIGHTS = 'flights/LIST_FLIGHTS';

export const listFlights = payload => ({
  type: LIST_FLIGHTS,
  payload: payload
});
