import { useState } from 'react'
import { useStateValue } from '../../index'
import { loadCompanies } from '../queries'
import { listCompanies } from '../actions'

const useCompanies = () => {
  const [{ companies }, dispatch] = useStateValue()
  const [isLoading, setIsLoading] = useState(false)

  const request = async () => {
    setIsLoading(true)

    const response = await loadCompanies();

    if (response) {
      dispatch(listCompanies(response))
    } else {
      const err = []
      dispatch(listCompanies(err))
    }
    setIsLoading(false)
  }

  return [companies, isLoading, request]
}

export default useCompanies
