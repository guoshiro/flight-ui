import React from 'react'
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom'
import { StateProvider } from '../state';
import { INITIAL_STATE as COMPANIES_INITIAL_STATE } from '../state/companies/reducers'
import { INITIAL_STATE as FLIGHT_INITIAL_STATE } from '../state/flights/reducers'
import reducers from '../state/reducers';
import BaseStyles from './base-styles';
import Content from '../components/content'
import Home from '../views/home'

const Root = props => {
  const initialState = {
    flights: FLIGHT_INITIAL_STATE,
    companies: COMPANIES_INITIAL_STATE,
  }
  return (
    <StateProvider initialState={initialState} reducer={reducers}>
      <BaseStyles />
      <Router>
        <>
          <Content>
            <Switch>
              <Route exact path='/' component={Home} />
            </Switch>
          </Content>
        </>
      </Router>
    </StateProvider>
  )
};

export default Root
