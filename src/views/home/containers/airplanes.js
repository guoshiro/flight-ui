import React, { useState } from 'react';
import styled from 'styled-components'

import Card from '../components/Card'
import Select from '../../../components/select'
import theme from '../../../components/theme';
import { orderByList } from '../../../constants';

const Container = styled.div`
    width: 60vw;
    display: block;
    padding: 5rem;
    overflow-x: auto;
`;

const Title = styled.div`
    display: flex;
    text-align: center;
    align-items: center;
    justify-content: center;
`

const Left = styled.h2`
    margin-right: 20px;
`

const Right = styled.h2`
`
const List = styled.div`
    width: 100%;
    border-bottom: 2px dashed ${theme.colors.grayMediumLight};
    padding: 40px 0px;
    margin: 10px 0px;
    :last-of-type{
        border-bottom: none;
    }
`



const Search = props => {

    return (
        <Container>
            <Select
                onChange={e => props.handlerFilter(e)}
                default={'Ordernar por..'}
                options={orderByList} />
            {props.flights && props.flights.length !== 0
                ? props.flights.map((flight, i) => {
                    return (
                        <List key={i}>
                            <Title>
                                <Left> Local de origem<br />{flight.origem}</Left>
                                <Right>Local de Destino <br />{flight.destino}</Right>
                            </Title>
                            <div>
                                {flight.voos.map((voo, j) => {
                                    return (
                                        <Card key={j} fly={voo} />
                                    )
                                })
                                }
                            </div>
                        </List>
                    )
                })
                : 'Desculpe, não há vôos para esses itinerários nesse momento. Por favor, verifique se há em outra data!'
            }
        </Container >
    )

}

export default Search;